package cdc.pstrings;

/**
 * Enumeration of possible packings.
 *
 * @author Damien Carbonne
 */
public enum StringPacking {
    PACKED,
    NONE
}