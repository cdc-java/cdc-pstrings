package cdc.pstrings;

import java.nio.ByteBuffer;

final class PackedString8 extends PackedString {
    private final int f1;
    private final int f2;

    public PackedString8(final byte[] bytes) {
        f1 = get(bytes, 3) | get(bytes, 2) << 8 | get(bytes, 1) << 16 | get(bytes, 0) << 24;
        f2 = get(bytes, 7) | get(bytes, 6) << 8 | get(bytes, 5) << 16 | get(bytes, 4) << 24;
    }

    @Override
    protected byte[] toBytes() {
        final ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putInt(f1);
        buffer.putInt(f2);
        return buffer.array();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PackedString8)) {
            return false;
        }
        final PackedString8 o = (PackedString8) other;
        return f1 == o.f1
                && f2 == o.f2;
    }

    @Override
    public int hashCode() {
        int result = f1;
        result = 31 * result + f2;
        return result;
    }
}