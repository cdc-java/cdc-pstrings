/**
 * Set of classes used to pack strings in more compact representations.
 * <p>
 * This is useful when size matters more than speed.
 * <p>
 * Strings are encoded in UTF-8. The resulting encoding is stored in specific classes:
 * <ul>
 * <li>When the number of bytes is small enough, bytes are stored in fields:
 * {@link cdc.pstrings.PackedString8 PackedString8},
 * {@link cdc.pstrings.PackedString16 PackedString16}, ...
 * <li>Otherwise, bytes are stored in an array: {@link cdc.pstrings.PackedStringN PackedStringN}.
 * </ul>
 *
 * @author Damien Carbonne
 */
package cdc.pstrings;
