package cdc.pstrings;

import java.util.Arrays;

final class PackedStringN extends PackedString {
    private final byte[] bytes;

    public PackedStringN(byte[] bytes) {
        this.bytes = new byte[bytes.length];
        for (int index = 0; index < bytes.length; index++) {
            this.bytes[index] = bytes[index];
        }
    }

    @Override
    protected byte[] toBytes() {
        return bytes;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PackedStringN)) {
            return false;
        }
        final PackedStringN o = (PackedStringN) other;
        return Arrays.equals(bytes, o.bytes);
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (final byte b : bytes) {
            result = 31 * result + b;
        }
        return result;
    }
}