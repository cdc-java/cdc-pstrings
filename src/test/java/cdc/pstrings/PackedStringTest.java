package cdc.pstrings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class PackedStringTest {

    private static String build(String s,
                                int count) {
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < count; index++) {
            builder.append(s);
        }
        return builder.toString();
    }

    private static void checkConvert(String s) {
        final Object o1 = PackedString.convert(s);
        if (s == null) {
            assertEquals(null, o1);
        } else {
            final String s2 = o1.toString();
            assertEquals(s, s2);
        }
        final Object o2 = PackedString.convert(s);
        assertEquals(o1, o2);
        if (o1 != null && o2 != null) {
            assertEquals(o1.hashCode(), o2.hashCode());
        }
    }

    private static void checkConvert(int n) {
        final char c = (char) n;
        if (!Character.isSurrogate(c)) {
            final String s = Character.toString(c);
            final Object o1 = PackedString.convert(s);
            final Object o2 = PackedString.convert(s, true);
            final Object o3 = PackedString.convert(s, false);
            final String s1p = o1.toString();
            final String s2p = o2.toString();
            final String s3p = o3.toString();
            assertEquals(s, s1p);
            assertEquals(s, s2p);
            assertEquals(s, s3p);
        }
    }

    @Test
    void checkConvert() {
        checkConvert(null);
        checkConvert("");
        checkConvert("a");
        checkConvert("à");
        for (int i = 0; i < 128; i++) {
            checkConvert(build("a", i));
            checkConvert(build("à", i));
        }

        for (int i = Character.MIN_CODE_POINT; i <= Character.MAX_CODE_POINT; i++) {
            checkConvert(i);
        }
    }

    @Test
    void checkEquals() {
        final List<String> s = new ArrayList<>();
        final List<Object> ps = new ArrayList<>();
        for (int i1 = 0; i1 < 70; i1++) {
            for (int k = 0; k <= i1; k++) {
                final StringBuilder builder = new StringBuilder();
                for (int i2 = 0; i2 < i1; i2++) {
                    if (k == i2) {
                        builder.append('1');
                    } else {
                        builder.append('0');
                    }
                }
                final String x = builder.toString();
                s.add(x);
                ps.add(PackedString.convert(x));
                // LOGGER.info(builder.toString());
            }
        }
        for (int i1 = 0; i1 < s.size(); i1++) {
            for (int i2 = 0; i2 < s.size(); i2++) {
                assertSame(s.get(i1).equals(s.get(i2)), ps.get(i1).equals(ps.get(i2)));
            }
        }
    }

    @Test
    void checkIntern() {
        assertSame(null, PackedString.intern(null));
        assertEquals("Hello", PackedString.intern("Hello"));
        assertEquals(PackedString.convert("Hello"), PackedString.intern(PackedString.convert("Hello")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         PackedString.intern('A');
                     });
    }
}