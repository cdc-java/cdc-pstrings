# cdc-pstrings

Classes used to encode strings in a compact way.  
This is memory efficient, but is computation intensive.

**Note:** This is probably not needed any more with recent JDK (compressed string JVM option in JDK6 / compact string implementation since Java 9).

**Note:** This code has been extracted from cdc-util.