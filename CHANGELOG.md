# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Updated maven plugins
- Upgraded to Java 11


## [1.0.4] - 2022-01-03
### Security
- Removed dependency on log4j. It was only used for tests. #1


## [1.0.3] - 2021-12-28
### Security
- Updated dependencies,
    - org.apache.log4j-2.17.0. #1


## [1.0.2] - 2021-12-14
### Added
- CHANGELOG.md
- COPYING.LESSER

### Changed
- Updated dependencies,
    - org.apache.log4j-2.16.0. #1

### Fixed


## [1.0.1] - 2021-02-28
### Added
- First usable release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util).  
  It has special handling of strings up to 64 bytes.